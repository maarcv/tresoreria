import MySQLdb
import datetime
import pyodbc

"""
    Agafem assentaments, cartera de cobraments, cartera de pagaments i apunts manuals i els 


"""


sqlSrv_server = '54.37.151.60'
sqlSrv_database = 'GlamERPDW'
sqlSrv_username = 'guest'
sqlSrv_password = 'glamerp'

mySql_server = '127.0.0.1'
mySql_port = 3307
mySql_database = 'glam'
mySql_username = 'root'
mySql_password = 'root'

select_data_obertura = """
    SELECT TOP 1 [Data]
    FROM [GlamERPDW].[dbo].[AccountingLog]
    where Diari = 'O' ORDER BY [Data] desc;
"""

select_saldo_banc = """
    SELECT [debe] - [haber] Import
    FROM [GlamERPDW].[dbo].[AccountingLog]
    where IdSubcompteFK = ? and Diari = 'O' and [Data] = ?;
"""

select_assentaments = """
    SELECT [Data], [IdPunteigFK], [debe] - [haber] Import, [Concepte] 
    FROM [GlamERPDW].[dbo].[AccountingLog]
    where [Data] >= ? and IdSubcompteFK = ? and Diari = 'N'
    order by [Data] asc;
"""

select_cartera_cobros = """
    SELECT cast([Duedate] as DATE) dates, [Import], [Entity]
    FROM [GlamERPDW].[dbo].[CollectionPortfolio] 
    where [Duedate] >= getdate() and [AccountId] = ?
    order by [Duedate] asc
"""

select_cartera_pagos = """
    SELECT cast([Duedate] as DATE) dates, -[Import] Import, [Entity]
    FROM [GlamERPDW].[dbo].[PaymentPortfolio]
    where [AccountId] = ?
    order by [Duedate] asc
"""


# ----------------------------------------------------------------------------------------------------------------------
#                                             Comencem
# ----------------------------------------------------------------------------------------------------------------------

ORIGEN_ID_ASSENTAMENT = 1
ORIGEN_ID_CARTERA = 2
ORIGEN_ID_MANUAL = 3

TIP_ID_SALDO_INICIAL = 1
TIP_ID_MOVIMENT_ENTRADA = 2
TIP_ID_MOVIMENT_SORTIDA = 3


sqlSrv_con = pyodbc.connect(
    'DRIVER={ODBC Driver 18 for SQL Server};SERVER=' + sqlSrv_server + ';DATABASE=' + sqlSrv_database +
    ';UID=' + sqlSrv_username + ';PWD=' + sqlSrv_password + ";TrustServerCertificate=YES"
)
sqlSrv_cursor = sqlSrv_con.cursor()
mySql_con = MySQLdb.Connection(
    user=mySql_username,
    passwd=mySql_password,
    host=mySql_server,
    db=mySql_database,
    charset='utf8',
    port=mySql_port
)
mySql_cur: MySQLdb.cursors.Cursor
mySql_cur = mySql_con.cursor()


hora_inici = datetime.datetime.now()
mySql_cur.execute("insert into importacions (hora) values (%s)", (hora_inici,))
importacio_id = mySql_cur.lastrowid
data_obertura = sqlSrv_cursor.execute(select_data_obertura).fetchone()[0]


mySql_cur.execute("select id, src_id_compte from bancs")
for banc_id, banc_src_id in mySql_cur.fetchall():
    saldo_inicial = sqlSrv_cursor.execute(select_saldo_banc, banc_src_id, data_obertura).fetchone()[0]

    # ------------------------------------------------------------------------------------------------------------------
    #                                      CARREGUEM ASSENTAMENTS
    # ------------------------------------------------------------------------------------------------------------------

    sqlSrv_cursor.execute(select_assentaments, data_obertura, banc_src_id)
    for r_data, r_punteig_id, r_import, r_descripcio in sqlSrv_cursor.fetchall():
        tmp_tip_moviment = TIP_ID_MOVIMENT_ENTRADA
        if r_import < 0:
            tmp_tip_moviment = TIP_ID_MOVIMENT_SORTIDA
        mySql_cur.execute(
            query="""
                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import, descripcio) 
                values (%s, %s, %s, %s, %s, round(%s, 2), %s)
            """,
            args=(importacio_id, ORIGEN_ID_ASSENTAMENT, tmp_tip_moviment, r_data, banc_id, r_import, r_descripcio)
        )

    # ------------------------------------------------------------------------------------------------------------------
    #                            CARREGUEM CARTERA DE COBRAMENTS I DE PAGAMENTS
    # ------------------------------------------------------------------------------------------------------------------

    sqlSrv_cursor.execute(select_cartera_cobros, banc_src_id)
    for r_data, r_import, r_descripcio in sqlSrv_cursor.fetchall():
        tmp_tip_moviment = TIP_ID_MOVIMENT_ENTRADA
        if r_import < 0:
            tmp_tip_moviment = TIP_ID_MOVIMENT_SORTIDA
        mySql_cur.execute(
            query="""
                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import, descripcio) 
                values (%s, %s, %s, %s, %s, round(%s, 2), %s)
            """,
            args=(importacio_id, ORIGEN_ID_CARTERA, tmp_tip_moviment, r_data, banc_id, r_import, r_descripcio)
        )

    sqlSrv_cursor.execute(select_cartera_pagos, banc_src_id)
    for r_data, r_import, r_descripcio in sqlSrv_cursor.fetchall():
        tmp_tip_moviment = TIP_ID_MOVIMENT_ENTRADA
        if r_import < 0:
            tmp_tip_moviment = TIP_ID_MOVIMENT_SORTIDA
        mySql_cur.execute(
            query="""
                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import, descripcio) 
                values (%s, %s, %s, %s, %s, round(%s, 2), %s)
            """,
            args=(importacio_id, ORIGEN_ID_CARTERA, tmp_tip_moviment, r_data, banc_id, r_import, r_descripcio)
        )

    # ------------------------------------------------------------------------------------------------------------------
    #                                      CARREGUEM MOVIMENTS MANUALS
    # ------------------------------------------------------------------------------------------------------------------

    mySql_cur.execute(
        query="select data, import, concepte from moviments_manuals where banc_id = %s and comptabilitzat = 0",
        args=(banc_id,)
    )
    for r_data, r_import, r_descripcio in mySql_cur.fetchall():
        tmp_tip_moviment = TIP_ID_MOVIMENT_ENTRADA
        if r_import < 0:
            tmp_tip_moviment = TIP_ID_MOVIMENT_SORTIDA
        mySql_cur.execute(
            query="""
                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import, descripcio) 
                values (%s, %s, %s, %s, %s, round(%s, 2), %s)
            """,
            args=(importacio_id, ORIGEN_ID_MANUAL, tmp_tip_moviment, r_data, banc_id, r_import, r_descripcio)
        )

    # ------------------------------------------------------------------------------------------------------------------
    #                                   AFEGIM SALDOS A TAULA MOVIMENTS
    # ------------------------------------------------------------------------------------------------------------------

    r_data_old = data_obertura
    r_import_assentaments = saldo_inicial
    r_import_carteres = 0
    r_import_manuals = 0

    mySql_cur.execute(
        query="""
                select data, origen_id, import
                from apunts
                where banc_id = %s AND importacio_id = %s order by `data` asc, import asc
            """,
        args=(banc_id, importacio_id,)
    )
    for r_data, r_origen_id, r_import in mySql_cur.fetchall():
        while r_data_old <= r_data:  # Amb aquest loop aconseguim tenir registres cada dia

            mySql_cur.execute(
                query="""
                    insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import) 
                    values (%s, %s, %s, %s, %s, round(%s, 2))
                """,
                args=(
                    importacio_id, ORIGEN_ID_ASSENTAMENT, TIP_ID_SALDO_INICIAL, r_data_old, banc_id,
                    r_import_assentaments
                )
            )
            mySql_cur.execute(
                query="""
                                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import) 
                                values (%s, %s, %s, %s, %s, round(%s, 2))
                            """,
                args=(importacio_id, ORIGEN_ID_CARTERA, TIP_ID_SALDO_INICIAL, r_data_old, banc_id, r_import_carteres)
            )
            mySql_cur.execute(
                query="""
                                insert into apunts (importacio_id, origen_id, tipus_id, data, banc_id, import) 
                                values (%s, %s, %s, %s, %s, round(%s, 2))
                            """,
                args=(importacio_id, ORIGEN_ID_MANUAL, TIP_ID_SALDO_INICIAL, r_data_old, banc_id, r_import_manuals)
            )
            r_data_old += datetime.timedelta(days=1)

        if r_origen_id == ORIGEN_ID_ASSENTAMENT:
            r_import_assentaments += r_import
        if r_origen_id == ORIGEN_ID_CARTERA:
            r_import_carteres += r_import
        if r_origen_id == ORIGEN_ID_MANUAL:
            r_import_manuals += r_import


mySql_cur.close()
mySql_con.commit()
mySql_con.close()
sqlSrv_cursor.close()
sqlSrv_con.close()
