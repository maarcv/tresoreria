-- MySQL dump 10.13  Distrib 5.7.37, for osx10.16 (x86_64)
--
-- Host: 127.0.0.1    Database: glam
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bancs`
--

DROP TABLE IF EXISTS `bancs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bancs` (
  `id` int NOT NULL,
  `nom` varchar(45) NOT NULL,
  `compte` varchar(45) NOT NULL,
  `src_id_compte` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bancs_uk1` (`nom`),
  UNIQUE KEY `banks_uk2` (`compte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `importacions`
--

DROP TABLE IF EXISTS `importacions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `importacions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moviments`
--

DROP TABLE IF EXISTS `moviments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moviments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `importacio_id` int NOT NULL,
  `data` date NOT NULL,
  `banc_id` int NOT NULL,
  `moviment_tipus_id` int NOT NULL,
  `import` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `moviments_bancs_fk_idx` (`banc_id`),
  KEY `moviments_movimentstip_fk_idx` (`moviment_tipus_id`),
  KEY `moviments_importacions_fk_idx` (`importacio_id`),
  CONSTRAINT `moviments_bancs_fk` FOREIGN KEY (`banc_id`) REFERENCES `bancs` (`id`),
  CONSTRAINT `moviments_importacions_fk` FOREIGN KEY (`importacio_id`) REFERENCES `importacions` (`id`),
  CONSTRAINT `moviments_movimentstip_fk` FOREIGN KEY (`moviment_tipus_id`) REFERENCES `moviments_tipus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=677 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moviments_manuals`
--

DROP TABLE IF EXISTS `moviments_manuals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moviments_manuals` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `import` double NOT NULL,
  `concepte` varchar(45) NOT NULL,
  `banc_id` int NOT NULL,
  `comptabilitzat` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `moviments_manuals_bancs_fk_idx` (`banc_id`),
  CONSTRAINT `moviments_manuals_bancs_fk` FOREIGN KEY (`banc_id`) REFERENCES `bancs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moviments_tipus`
--

DROP TABLE IF EXISTS `moviments_tipus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moviments_tipus` (
  `id` int NOT NULL,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saldos`
--

DROP TABLE IF EXISTS `saldos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saldos` (
  `importacio_id` int NOT NULL,
  `data` date NOT NULL,
  `banc_id` int NOT NULL,
  `puntejat` double NOT NULL,
  `previst` double NOT NULL,
  `previst_minim` double NOT NULL,
  `previst_no_vendes` double NOT NULL,
  PRIMARY KEY (`importacio_id`,`data`,`banc_id`),
  KEY `saldos_bancs_fk_idx` (`banc_id`),
  KEY `saldos_importacions_fk_idx` (`importacio_id`),
  CONSTRAINT `saldos_bancs_fk` FOREIGN KEY (`banc_id`) REFERENCES `bancs` (`id`),
  CONSTRAINT `saldos_importacions_fk` FOREIGN KEY (`importacio_id`) REFERENCES `importacions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-01 16:59:25
